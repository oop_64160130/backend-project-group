import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  UseGuards,
} from '@nestjs/common';
import { SummarySalaryService } from './summary_salary.service';
import { CreateSummarySalaryDto } from './dto/create-summary_salary.dto';
import { UpdateSummarySalaryDto } from './dto/update-summary_salary.dto';

import { Role } from 'src/types/Role';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';

@Controller('summary-salary')
export class SummarySalaryController {
  constructor(private readonly summarySalaryService: SummarySalaryService) {}

  @Post()
  create(@Body() createSummarySalaryDto: CreateSummarySalaryDto) {
    return this.summarySalaryService.create(createSummarySalaryDto);
  }

  @Get()
  findAll(@Query() query: { cat?: string }) {
    return this.summarySalaryService.findAll(query);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.summarySalaryService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateSummarySalaryDto: UpdateSummarySalaryDto,
  ) {
    return this.summarySalaryService.update(+id, updateSummarySalaryDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.summarySalaryService.remove(+id);
  }

  @Get('employee/:id')
  findOneByEmployee(@Param('id') id: string) {
    return this.summarySalaryService.findOneByEmployee(+id);
  }
}
