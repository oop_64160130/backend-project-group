import { Module } from '@nestjs/common';
import { EmployeesService } from './employees.service';
import { EmployeesController } from './employees.controller';
import { Employee } from './entities/employee.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CheckMaterial } from 'src/check_material/entities/check_material.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Employee, CheckMaterial])],
  controllers: [EmployeesController],
  providers: [EmployeesService],
})
export class EmployeesModule {}
