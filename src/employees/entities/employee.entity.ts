import { Bill } from 'src/bills/entities/bill.entity';
import { User } from 'src/users/entities/user.entity';
import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  OneToMany,
  OneToOne,
} from 'typeorm';

@Entity()
export class Employee {
  [x: string]: any;
  @PrimaryGeneratedColumn({ name: 'employee_id' })
  id: number;

  @Column({ name: 'employee_name' })
  name: string;

  @Column()
  address: string;

  @Column()
  tel: string;

  @Column()
  email: string;

  @Column()
  position: string;

  @Column()
  hourly: number;

  @Column({
    length: '128',
    default: 'No-Image-Placeholder.jpg',
  })
  image: string;
  @Column({ default: 9000 })
  salary: number;
  @Column({ default: true })
  fullTime: boolean;
  @OneToOne(() => User, (user) => user.employee)
  user: User;

  // @OneToMany(() => Bill, (bill) => bill.employee)
  // bill: Bill;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;
}
