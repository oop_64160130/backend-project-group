import { IsNotEmpty, Length } from 'class-validator';
export class CreateEmployeeDto {
  @IsNotEmpty()
  name: string;
  @IsNotEmpty()
  address: string;
  @IsNotEmpty()
  tel: string;
  @IsNotEmpty()
  email: string;
  @IsNotEmpty()
  position: string;
  @IsNotEmpty()
  hourly: number;

  image = 'No-Image-Placeholder.jpg';
}
