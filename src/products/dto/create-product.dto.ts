import { IsNotEmpty, Length } from 'class-validator';

export class CreateProductDto {
  @IsNotEmpty()
  @Length(3, 32)
  name: string;

  @IsNotEmpty()
  type: string;

  @IsNotEmpty()
  size: string;

  @IsNotEmpty()
  price: number;

  image = 'No-Image-Placeholder.jpg';

  @IsNotEmpty()
  categoryId: number;
}
