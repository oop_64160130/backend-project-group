import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
} from '@nestjs/common';
import { ReportsService } from './reports.service';
import { CreateReportDto } from './dto/create-report.dto';
import { UpdateReportDto } from './dto/update-report.dto';

@Controller('reports')
export class ReportsController {
  constructor(private readonly reportsService: ReportsService) {}

  @Get('/product')
  getProduct(
    @Query()
    query: {
      product_name?: string;
      lowPrice?: number;
      upperPrice?: number;
      searchText?: string;
    },
  ): Promise<any> {
    console.log(query);
    if (query.searchText) {
      return this.reportsService.getProductBySearchText(query.searchText);
    }
    return this.reportsService.getProduct();
  }

  @Get('/customer')
  getCustomer(
    @Query()
    query: {
      customer_name?: string;
      searchText?: string;
    },
  ) {
    console.log(query);
    if (query.searchText) {
      return this.reportsService.getCustomerBySearchText(query.searchText);
    }
    return this.reportsService.getCustomer();
  }

  @Get('/user')
  getUser(
    @Query()
    query: {
      user_name?: string;
      searchText?: string;
    },
  ) {
    console.log(query);
    if (query.searchText) {
      return this.reportsService.getUserBySearchText(query.searchText);
    }
    return this.reportsService.getUser();
  }

  @Get('/material')
  getMaterial(
    @Query()
    query: {
      material_name?: string;
      searchText?: string;
    },
  ) {
    console.log(query);
    if (query.searchText) {
      return this.reportsService.getMaterialBySearchText(query.searchText);
    }
    return this.reportsService.getMaterial();
  }

  @Get('/employee')
  getEmployee(
    @Query()
    query: {
      employee_name?: string;
      searchText?: string;
    },
  ) {
    console.log(query);
    if (query.searchText) {
      return this.reportsService.getEmployeeBySearchText(query.searchText);
    }
    return this.reportsService.getEmployee();
  }

  @Get('/orderItem')
  getOrderItem(
    @Query()
    query: {
      orderitem_name?: string;
      searchText?: string;
    },
  ) {
    console.log(query);
    if (query.searchText) {
      return this.reportsService.getOrderItemBySearchText(query.searchText);
    }
    return this.reportsService.getOrderItem();
  }

  @Get('/categories')
  getCategories(
    @Query()
    query: {
      categories_name?: string;
      searchText?: string;
    },
  ) {
    console.log(query);
    if (query.searchText) {
      return this.reportsService.getCategoriesBySearchText(query.searchText);
    }
    return this.reportsService.getCategories();
  }

  @Post()
  create(@Body() createReportDto: CreateReportDto) {
    return this.reportsService.create(createReportDto);
  }

  @Get()
  findAll() {
    return this.reportsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.reportsService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateReportDto: UpdateReportDto) {
    return this.reportsService.update(+id, updateReportDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.reportsService.remove(+id);
  }
}
