import { Injectable } from '@nestjs/common';
import { CreateReportDto } from './dto/create-report.dto';
import { UpdateReportDto } from './dto/update-report.dto';
import { InjectDataSource } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';

@Injectable()
export class ReportsService {
  constructor(@InjectDataSource() private dataSource: DataSource) {}

  getProductBySearchText(searchText: string) {
    return this.dataSource.query(
      'SELECT * FROM `getProduct_info` WHERE product_name LIKE ?',
      [`%${searchText}%`],
    );
  }

  getProduct() {
    return this.dataSource.query('CALL `getProduct`()');
  }

  getOrderItemBySearchText(searchText: string) {
    return this.dataSource.query(
      'SELECT * FROM `getOrderItem_info` WHERE orderitem_name LIKE ?',
      [`%${searchText}%`],
    );
  }

  getOrderItem() {
    return this.dataSource.query('CALL `getOrderItem`()');
  }

  getUserBySearchText(searchText: string) {
    return this.dataSource.query(
      'SELECT * FROM `getUser_info` WHERE user_name LIKE ?',
      [`%${searchText}%`],
    );
  }

  getUser() {
    return this.dataSource.query('CALL `getUser`()');
  }

  getCustomerBySearchText(searchText: string) {
    return this.dataSource.query(
      'SELECT * FROM `getCustomer_info` WHERE customer_name LIKE ?',
      [`%${searchText}%`],
    );
  }

  getCustomer() {
    return this.dataSource.query('CALL `getCustomer`()');
  }

  getMaterialBySearchText(searchText: string) {
    return this.dataSource.query(
      'SELECT * FROM `getMaterial_info` WHERE material_name LIKE ?',
      [`%${searchText}%`],
    );
  }

  getMaterial() {
    return this.dataSource.query('CALL `getMaterial`()');
  }
  getEmployeeBySearchText(searchText: string) {
    return this.dataSource.query(
      'SELECT * FROM `getEmployee_info` WHERE employee_name LIKE ?',
      [`%${searchText}%`],
    );
  }

  getEmployee() {
    return this.dataSource.query('CALL `getEmloyee`()');
  }

  getCategoriesBySearchText(searchText: string) {
    return this.dataSource.query(
      'SELECT * FROM `getCategory_info` WHERE category_name LIKE ?',
      [`%${searchText}%`],
    );
  }

  getCategories() {
    return this.dataSource.query('CALL `getCategory`()');
  }

  create(createReportDto: CreateReportDto) {
    return 'This action adds a new report';
  }

  findAll() {
    return `This action returns all reports`;
  }

  findOne(id: number) {
    return `This action returns a #${id} report`;
  }

  update(id: number, updateReportDto: UpdateReportDto) {
    return `This action updates a #${id} report`;
  }

  remove(id: number) {
    return `This action removes a #${id} report`;
  }
}
