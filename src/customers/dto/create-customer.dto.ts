import { IsNotEmpty } from 'class-validator';

export class CreateCustomerDto {
  @IsNotEmpty()
  name: string;
  @IsNotEmpty()
  tel: string;
  @IsNotEmpty()
  point: number;
  image = 'No-Image-Placeholder.jpg';
}
