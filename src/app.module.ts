import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { Product } from './products/entities/product.entity';
import { ProductsModule } from './products/products.module';
import { User } from './users/entities/user.entity';
import { UsersModule } from './users/users.module';
import { OrdersModule } from './orders/orders.module';
import { CustomersModule } from './customers/customers.module';
import { CategoriesModule } from './categories/categories.module';
import { Customer } from './customers/entities/customer.entity';
import { Order } from './orders/entities/order.entity';
import { OrderItem } from './orders/entities/order-item';
import { Category } from './categories/entities/category.entity';
import { EmployeesModule } from './employees/employees.module';
import { Employee } from './employees/entities/employee.entity';
import { MaterialsModule } from './materials/materials.module';
import { AuthModule } from './auth/auth.module';
import { Material } from './materials/entities/material.entity';
import { ReportsModule } from './reports/reports.module';

import { BillsModule } from './bills/bills.module';
import { CheckMaterialModule } from './check_material/check_material.module';
import { Bill } from './bills/entities/bill.entity';

import { CheckMaterial } from './check_material/entities/check_material.entity';

import { ReportCusModule } from './report-cus/report-cus.module';
import { BillDetail } from './bill_detail/entities/bill_detail.entity';
import { CheckInOut } from './check_in_outs/entities/check_in_out.entity';
import { CheckMaterialDetail } from './check_material_detail/entities/check_material_detail.entity';
import { SummarySalary } from './summary_salary/entities/summary_salary.entity';
import { Store } from './stores/entities/store.entity';

@Module({
  imports: [
    ProductsModule,
    TypeOrmModule.forRoot(
      {
        //Username : guest03
        //Password xE6ErtSt
        //https://www.db4free.net/phpMyAdmin/
        //https://angsila.informatics.buu.ac.th/phpmyadmin/

        // type: 'mysql',
        // host: 'angsila.informatics.buu.ac.th',
        // port: 3306,
        // username: 'guest03',
        // password: 'xE6ErtSt',
        // database: 'guest03',

        type: 'mysql',
        host: 'db4free.net',
        port: 3306,
        username: 'coffee_seven',
        password: 'Pass@1234',
        database: 'coffee_seven_db',
        entities: [
          Product,
          User,
          Customer,
          Order,
          OrderItem,
          Category,
          Material,
          Employee,
          Bill,
          BillDetail,
          CheckInOut,
          CheckMaterial,
          CheckMaterialDetail,
          SummarySalary,
          Store,
        ],
        synchronize: true,
      },
      // {
      //   type: 'sqlite',
      //   database: 'db.sqlite',
      //   entities: [
      //     Product,
      //     User,
      //     Customer,
      //     Order,
      //     OrderItem,
      //     Category,
      //     Employee,
      //   ],
      //   migrations: [],
      //   synchronize: true,
      // },
    ),
    UsersModule,
    OrdersModule,
    CustomersModule,
    CategoriesModule,
    EmployeesModule,
    MaterialsModule,
    AuthModule,
    ReportsModule,

    BillsModule,
    CheckMaterialModule,
    ReportCusModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
