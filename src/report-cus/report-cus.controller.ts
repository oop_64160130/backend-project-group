import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
} from '@nestjs/common';
import { ReportCusService } from './report-cus.service';
import { CreateReportCusDto } from './dto/create-report-cus.dto';
import { UpdateReportCusDto } from './dto/update-report-cus.dto';

@Controller('report-cus')
export class ReportCusController {
  constructor(private readonly reportCusService: ReportCusService) {}
  @Get('/customer')
  getCustomer() {
    return this.reportCusService.getCustomer();
  }
  @Get('/delcustomer')
  delCustomer() {
    return this.reportCusService.delCustomer();
  }
  // @Get('/customer')
  // getCustomer(
  //   @Query()
  //   query: {
  //     customer_name?: string;
  //     searchText?: string;
  //   },
  // ) {
  //   console.log(query);
  //   if (query.searchText) {
  //     return this.reportCusService.getCustomerBySearchText(query.searchText);
  //   }
  //   return this.reportCusService.getCustomer();
  // }

  @Post()
  create(@Body() createReportCusDto: CreateReportCusDto) {
    return this.reportCusService.create(createReportCusDto);
  }

  @Get()
  findAll() {
    return this.reportCusService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.reportCusService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateReportCusDto: UpdateReportCusDto,
  ) {
    return this.reportCusService.update(+id, updateReportCusDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.reportCusService.remove(+id);
  }
}
