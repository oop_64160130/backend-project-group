import { Test, TestingModule } from '@nestjs/testing';
import { ReportCusService } from './report-cus.service';

describe('ReportCusService', () => {
  let service: ReportCusService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ReportCusService],
    }).compile();

    service = module.get<ReportCusService>(ReportCusService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
