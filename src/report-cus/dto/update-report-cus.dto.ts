import { PartialType } from '@nestjs/mapped-types';
import { CreateReportCusDto } from './create-report-cus.dto';

export class UpdateReportCusDto extends PartialType(CreateReportCusDto) {}
