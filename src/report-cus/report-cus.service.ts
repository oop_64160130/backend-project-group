import { Injectable } from '@nestjs/common';
import { CreateReportCusDto } from './dto/create-report-cus.dto';
import { UpdateReportCusDto } from './dto/update-report-cus.dto';
import { InjectDataSource } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';

@Injectable()
export class ReportCusService {
  constructor(@InjectDataSource() private dataSource: DataSource) {}

  getCustomer() {
    return this.dataSource.query('SELECT * FROM getCustomer_info'); //showView
  }
  delCustomer() {
    return this.dataSource.query('CALL DelCustomer'); //Storepo
  }

  getCustomerBySearchText(searchText: any) {
    return this.dataSource.query('SELECT * FROM getCustomer_info()');
  }

  create(createReportCusDto: CreateReportCusDto) {
    return 'This action adds a new reportCus';
  }

  findAll() {
    return `This action returns all reportCus`;
  }

  findOne(id: number) {
    return `This action returns a #${id} reportCus`;
  }

  update(id: number, updateReportCusDto: UpdateReportCusDto) {
    return `This action updates a #${id} reportCus`;
  }

  remove(id: number) {
    return `This action removes a #${id} reportCus`;
  }
}
