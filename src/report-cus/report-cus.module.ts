import { Module } from '@nestjs/common';
import { ReportCusService } from './report-cus.service';
import { ReportCusController } from './report-cus.controller';

@Module({
  controllers: [ReportCusController],
  providers: [ReportCusService],
})
export class ReportCusModule {}
