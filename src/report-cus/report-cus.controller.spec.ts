import { Test, TestingModule } from '@nestjs/testing';
import { ReportCusController } from './report-cus.controller';
import { ReportCusService } from './report-cus.service';

describe('ReportCusController', () => {
  let controller: ReportCusController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ReportCusController],
      providers: [ReportCusService],
    }).compile();

    controller = module.get<ReportCusController>(ReportCusController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
