import {
  IsNotEmpty,
  Length,
  Matches,
  IsString,
  IsPhoneNumber,
  IsPositive,
  Min,
  IsBoolean,
} from 'class-validator';
export class CreateUserDto {
  @IsNotEmpty()
  username: string;
  telEmployee: string;
  @IsNotEmpty()
  login: string;
  addressEmployee: string;
  @IsNotEmpty()
  role: string;
  @IsNotEmpty()
  @IsString()
  tel: string;
  @IsNotEmpty()
  @Min(12000)
  salary: number;
  @IsNotEmpty()
  address: string;
  @IsNotEmpty()
  @IsBoolean()
  fullTime: boolean;
  @IsNotEmpty()
  name_employee: string;
  @IsNotEmpty()
  @Matches(
    /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/,
  )
  password: string;

  @IsNotEmpty()
  hourly: number;

  @IsNotEmpty()
  @IsString()
  position?: string;

  image = 'No-Image-Placeholder.jpg';
}
